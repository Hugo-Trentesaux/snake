import unittest
from main import Point

class Test(unittest.TestCase):

    def test_pointEqual(self):
        self.assertEqual(Point(1,1),Point(1,1))
    def test_pointNotEqual(self):
        self.assertNotEqual(Point(1,2),Point(1,1))

if __name__ == '__main__':
    unittest.main()
