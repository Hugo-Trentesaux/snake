import random
import tkinter
import time

# === Parameters ===
class Parameters():
    """the class Parameters stores game parameters and keybindings
    """
    mapWidth = 16 # squares
    mapHeight = mapWidth
    squareSize = 32 # pixels
    speed = 20 # squares per seconds
    appleProbability = 1e-1/speed
    # key bindings
    bindings = {
        'up': 'z',
        'left': 'q',
        'down': 's',
        'right': 'd',
        'exit': '<Escape>',
        'pause': '<space>',
    }
bindings = Parameters.bindings # alias for parameters

# === Small utils ===
class Direction():
    """like an enumerate, but improved
    """
    class Vector():
        """minimal implementation of Vector
        """
        def __init__(self, x, y):
            self.x = x
            self.y = y
        def __eq__(self, other):
            return (self.x == other.x) and (self.y == other.y)
        def __neg__(self):
            return Direction.Vector(-self.x, -self.y)
    N = Vector(0,-1)
    S = Vector(0,+1)
    E = Vector(+1,0)
    W = Vector(-1,0)

class Point():
    """a point on a 2D grid, can be moved on a map with periodic conditions
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def moved(self, direction, map): # returns a new point moved with periodic conditions
        x, y = self.x, self.y
        if direction == Direction.N:
            y -= 1
        if direction == Direction.S:
            y += 1
        if direction == Direction.E:
            x += 1
        if direction == Direction.W:
            x -= 1
        x %= map.width
        y %= map.height
        return Point(x, y)
    def __eq__(self, point):
        return (self.x == point.x) and (self.y == point.y)
    def __repr__(self):
        return "(%d,%d)" % (self.x, self.y)

# === Game classes ===
class Map():
    """contain map width and height and methods to deal with points
    """
    def __init__(self):
        self.width = Parameters.mapWidth
        self.height = Parameters.mapHeight

    def randomPoint(self):
        x, y = (random.randrange(self.width), random.randrange(self.height))
        return Point(x,y)
    def middlePoint(self):
        x, y = self.width//2, self.height//2
        return Point(x,y)

class Snake():
    """the snake contains references on the game object and methods to evolve in its environment
    """
    def __init__(self, game):
        self.game = game
        self.apples = game.apples
        self.map = game.map # reference on snake's map
        self.direction = Direction.N
        self.alive = True
        self.body = [self.map.middlePoint()] # list of points
        for i in range(9): # do a 9 size body
            self.body.append(self.body[i].moved(Direction.S, self.map))
    def head(self):
        return self.body[0]
    def changeDirection(self, direction):
        if self.head().moved(direction, self.map) not in self.body: # prevent colliding
            self.direction = direction
    def move(self):
        newHead = self.head().moved(self.direction, self.map)
        if newHead in self.body:
            self.kill() # kills snake
        else:
            self.body.insert(0, newHead) # moves the head
        if newHead in self.apples: # if eating an apple
            self.apples.remove(newHead)
            self.game.spawnApple()
        else:
            self.body.pop(-1) # removes the last one
    def kill(self):
        self.alive = False

class Game():
    """contains the map, the snake, and the apples
    """
    def __init__(self):
        self.map = Map()
        self.apples = [self.map.randomPoint()]
        self.snake = Snake(self)
    def spawnApple(self):
        self.apples.append(self.map.randomPoint())
    def tryApple(self):
        if random.uniform(0,1) < Parameters.appleProbability:
            self.spawnApple()

# === GUI classes ===

pointToSquare = lambda point : tuple(Parameters.squareSize * x for x in [point.x, point.y, point.x+1, point.y+1])
middleMap = lambda map : (map.width*Parameters.squareSize/2, map.height*Parameters.squareSize/2)

class GuiApp(Game):
    """contains all methods to deal with GUI
    """
    def __init__(self):
        self.exit = False
        self.pause = True

        # inits the game
        super().__init__()

        # inits the gui
        self.root = tkinter.Tk()
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self.root.geometry("%dx%d" % (self.map.width * Parameters.squareSize + 40,
            self.map.height * Parameters.squareSize + 40))
        self.canvas = tkinter.Canvas(self.root)
        self.canvas.grid(sticky=tkinter.NSEW, padx=10, pady=10)

        # bindings
        orient = lambda direction: self.snake.changeDirection(direction)
        self.root.bind(bindings['up'], lambda event : orient(Direction.N))
        self.root.bind(bindings['left'], lambda event : orient(Direction.W))
        self.root.bind(bindings['down'], lambda event : orient(Direction.S))
        self.root.bind(bindings['right'], lambda event : orient(Direction.E))

        self.root.bind(bindings['exit'], lambda event : self.stopGame())
        self.root.bind(bindings['pause'], lambda event : self.tooglePause())

        # draw map and run game
        self.drawBase()
        self.drawUpdate()
        self.greeting()
        self.root.update()
        self.run()

    def stopGame(self):
        self.exit = True
    def pauseGame(self):
        self.pause = True
    def tooglePause(self):
        self.pause = not self.pause

    def gameOver(self):
        self.pauseGame()
        text = "Game Over"
        text += "\nscore: %d" % len(self.snake.body)
        self.canvas.create_text(middleMap(self.map), text=text, font=("Arial", 42), fill="blue", tags="message")

    def greeting(self):
        text = "Snake game"
        text += "\npress space to\nstart the game"
        self.canvas.create_text(middleMap(self.map), text=text, font=("Arial", 42), fill="blue", tags="message")

    def drawBase(self):
        """draws fixed content (square and grid)
        """
        # draw main square
        self.canvas.create_rectangle(0, 0, self.map.width*Parameters.squareSize, self.map.height*Parameters.squareSize)
        # draw a 2D grid
        for i in range(self.map.width):
            for j in range(self.map.height):
                self.canvas.create_rectangle( pointToSquare(Point(i,j)), outline="gray")

    def drawUpdate(self):
        """draws moving content (apples, snake)
        """
        # delete all moving
        self.canvas.delete("snake", "apple", "message")
        # draw apples
        for apple in self.apples:
            tmp = self.canvas.create_rectangle( pointToSquare(apple), fill="red")
            self.canvas.itemconfig(tmp, tags="apple")
        # draw snake
        for body in self.snake.body:
            tmp = self.canvas.create_rectangle( pointToSquare(body), fill="black" )
            self.canvas.itemconfig(tmp, tags="snake")

    def run(self):
        """runs the game in the main loop
        """
        while not self.exit: # while not exiting, run the loop and update
            if not self.snake.alive: # if snake is dead
                self.gameOver() # display game over
            if self.pause: # if game is paused
                self.root.update()
                time.sleep(0.1)
            if self.snake.alive and not self.pause: # if snake is alive and game is not paused
                tic = time.time() # get current time
                self.snake.move() # moves snake (or kill it)
                self.drawUpdate() # updates canvas
                self.root.update() # refresh
                tmp = 1/Parameters.speed - (time.time() - tic)
                time.sleep( tmp if tmp > 0 else 0 ) # wait to prevent going too fast

# === if main, do GUI ===
if __name__ == "__main__":
    game = GuiApp()
